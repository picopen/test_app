module ProviderService
  module_function

  def init(provider_name, options, params)
    klass = "#{provider_name.titleize}_Service".constantize # Ikigai_Service, Bank1_Service...
    klass.new(options, params)
  rescue 
    raise PROVIDER_NOT_REGISTERED
  end
end