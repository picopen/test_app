class Bank1Service < BaseService
  EVENT_KEY = "event_id"
  EVENTS_MAPPING = {
    "27" => Transaction::IN_STORE_TRANSACTION, 
    "31" => Transaction::ONLINE_TRANSACTION,   
    "2"  => Transaction::ATM_WITHDRAWAL,       
  }
end