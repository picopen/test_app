class BaseService
  def initialize(attributes, raw_data)
    @attributes = attributes
    @raw_data   = raw_data
  end

  def create_transaction!
    t = Transaction.new(attributes)
    t.event    = internal_event_name
    t.raw_data = @raw_data
    t.save!
  end

  private

  def internal_event_name
    event_key = @raw_data[EVENT_KEY]
    EVENTS_MAPPING[event_key]
  end
end