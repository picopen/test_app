class IkigaiService < BaseService
  EVENT_KEY = "event"
  EVENTS_MAPPING = {
    "in-person" => Transaction::IN_STORE_TRANSACTION, 
    "online" => Transaction::ONLINE_TRANSACTION,   
    "withdrawal"  => Transaction::ATM_WITHDRAWAL,       
  }
end