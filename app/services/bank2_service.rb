class Bank2Service < BaseService
  EVENT_KEY = "action"
  EVENTS_MAPPING = {
    "store-transaction"     => Transaction::IN_STORE_TRANSACTION, 
    "ecommerce-transaction" => Transaction::ONLINE_TRANSACTION,   
    "atm"                   => Transaction::ATM_WITHDRAWAL,       
  }
end