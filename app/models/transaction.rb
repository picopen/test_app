class Transaction < ApplicationRecord
  belongs_to :user
  belongs_to :provider

  # internal transaction events
  IN_STORE_TRANSACTION = "in-store-transaction"
  ONLINE_TRANSACTION   = "online-transaction"
  ATM_WITHDRAWAL       = "atm-withdrawal"
  enum event: [IN_STORE_TRANSACTION, ONLINE_TRANSACTION, ATM_WITHDRAWAL]

  validates :raw_data # the original data sent from provider
end