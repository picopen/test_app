class ApplicationController < ActionController::API
  def authorize_request!
    if headers["ACCESS_TOKEN"]
      # decrypt
      @token = headers["ACCESS_TOKEN"]
    else
      raise ERROR::REST::UNATHORIZED
    end
  end

  private

  rescue_from ERROR::REST::UNATHORIZED do
    head 401
  end

  rescue_from PROVIDER_NOT_REGISTERED do
    head 422
  end

  rescue_from ActiveRecord::RecordNotFound  do
    head 404
  end
end
