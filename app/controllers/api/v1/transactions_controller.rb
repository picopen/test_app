class Api::V1::TransactionsController < ApplicationController
  before_each :authorize_request!

  def create
    transaction_id = params.require(:transaction_id)
    user_guid      = params.require(:customer_id) 
    t = Transaction.find_by(id: transaction_id)

    if t.nil?
      options = {
        id:         transaction_id,
        user_id:    User.find_by!(guid: user_guid).id,
        created_at: params.require(:transaction_time),
      }

      provider_service = ProviderService.init(provider_name, options, params)
      provider_service.create_transaction!(options)
    end

    head :ok
  end

  private

  def provider_name
    @token["provider_key"]
  end
end